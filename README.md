# simulate_and_save

This repository contains sniptset of code that one can adapt to run simulation and save the results. 

The file `simulate_and_save` runs many simulation and output a confidence interval of the form +- $2\sqrt{var[X]/N}$, where $var[X]$ is the empirical variance and $N$ is the number of samples. 

The file `simulate_and_save_one_replica` runs a single simulation and saves the result. 

## Note

Copyright: Nicolas Gast, 2019 (nicolas.gast@inria.fr)

Code can probably be improved. 